Facter.add(:packer_version) do
  packer = '/usr/local/packer/packer'
  setcode do
    if File.exist? packer
      output = %x{#{packer} version}.strip
      
      # Sample output: Packer v0.6.0
      #   Match 1: version number without the 'v'
      /\D+([\d\.]+)/.match(output)[1]
    end
  end
end
