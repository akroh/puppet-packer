# == Class: packer
#
# Downloads and installs [Packer](https://packer.io). Packer is
# a tool for creating identical machine images for multiple platforms
# from a single source configuration.
#
# === Parameters
#
# [*version*]
#   Version number to install. Defaults to '0.7.5'. But it is strongly
#   recommended that you specify a version number in case we upgrade
#   the default value.
#
# [*sha256*]
#   Optional sha256 hash of the downloaded zip file. If not specified
#   then the checksum will not be verified. You can find checksums of
#   all files here https://dl.bintray.com/mitchellh/packer/.
#
# [*url*]
#   Optional URL from where the packer zip file should be obtained. If
#   not provided then the version specified will be downloaded from
#   bintray.
#
class packer (
  $version          = '0.7.5',
  $sha256           = undef,
  $url              = undef,
  $install_dir      = '/usr/local/packer',
  $download_dest    = '/tmp',
  $allow_insecure   = false,
  $follow_redirects = true,
  $timeout          = 120,
  $verbose          = true,
) {

  validate_re($version, '^(\d+\.)*(\d+)$')

  # Download from bintray if no URL is specified:
  if empty($url) {
    $kernel = downcase($::kernel)
    $arch   = $::architecture ? {
      'amd64'  => 'amd64',
      'x86_64' => 'amd64',
      'x86'    => '386',
      'i386'   => '386',
      default  => 'arm'
    }

    if versioncmp($version, '0.7.0') <= 0 or $version == '0.7.1' {
      $file = "${version}_${kernel}_${arch}.zip"
    } else {
      $file = "packer_${version}_${kernel}_${arch}.zip"
    }

    $_url = "http://dl.bintray.com/mitchellh/packer/${file}?direct"
  } else {
    $_url = $url
  }

  # Download and install only if the version installed
  # does not match the target version.
  if $version != $::packer_version {

    $filename = "packer-${version}.zip"

    $install_cmd = join([
      # Extract the zip to /tmp/packer
      'rm -rf /tmp/packer',
      'mkdir /tmp/packer',
      "unzip -o ${download_dest}/${filename} -d /tmp/packer",
      # Blow away any existing version if there is one
      "rm -rf ${install_dir}",
      # Move the directory to the install dir
      "mv /tmp/packer ${install_dir}",
      # Set ownership and permissions.
      "chown -R root:root ${install_dir}",
      "chmod -R 0555 ${install_dir}",
    ], ' && ')

    if empty($sha256) {
      $_checksum = false

      # Without a checksum, wipe any previous downloads first.
      exec { "Cleanup previously downloaded ${filename}":
        command => join([
          "rm -f ${download_dest}/${filename}",
          "rm -f ${download_dest}/${filename}.sha256",
        ], ' && '),
        user    => root,
        path    => $::path,
        onlyif  => join([
          "test -f ${download_dest}/${filename}",
          "-o -f ${download_dest}/${filename}.sha256",
        ], ' '),
      }
    } else {
      $_checksum = true
    }

    archive::download { $filename:
      ensure           => present,
      url              => $_url,
      timeout          => $timeout,
      checksum         => $_checksum,
      digest_type      => 'sha256',
      digest_string    => $sha256,
      src_target       => $download_dest,
      allow_insecure   => $allow_insecure,
      follow_redirects => $follow_redirects,
      verbose          => $verbose,
    }->

    exec { "Install packer ${version}":
      command   => $install_cmd,
      user      => root,
      path      => $::path,
      logoutput => on_failure,
    }->

    exec { "Cleanup downloaded ${filename}":
      command => join([
        "rm -f ${download_dest}/${filename}",
        "rm -f ${download_dest}/${filename}.sha256",
      ], ' && '),
      user    => root,
      path    => $::path,
      onlyif  => join([
        "test -f ${download_dest}/${filename}",
        "-o -f ${download_dest}/${filename}.sha256",
      ], ' '),
    }

    # Add packer to everyone's PATH.
    file { '/etc/profile.d/packer.sh':
      ensure  => present,
      content => 'export PATH=/usr/local/packer:$PATH',
      owner   => root,
      group   => root,
      mode    => '0444',
    }

  }

}
